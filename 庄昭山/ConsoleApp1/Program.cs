﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            string connectionString = "server=.;database=dbtest;uid=sa;pwd=123wan520";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from stuInfo";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("第一种数据查询");
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "stuId", "名字", "年龄", "性别");
            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["stuId"], sdr["stuName"], sdr["stuAge"], sdr["stuSex"]);
            }

            sqlConnection.Close();

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            
            
            DataBaseHelper hp = new DataBaseHelper();
            Console.WriteLine("第二种数据查询");
            hp.PrintUserInfoAdaper();

            
            Console.WriteLine( );
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("第三种数据查询");
           
            DataBaseHelper.PrintUser();
        }
        
    }
}
