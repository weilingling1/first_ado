﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class DataBaseHelper
    {
        private readonly string SqlCommandString= "select * from class";

        private readonly string connectionString = "server=.;database=SCHOOL;uid=sa;pwd=040520";

        public static void PrintUserInfo()
        {
            //连接数据库
            string connectionString = "server=.;database=SCHOOL;uid=sa;pwd=040520";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            //声明指令对象
            var SqlCommandString = "select * from class";

            SqlCommand sqlCommand = new SqlCommand(SqlCommandString, sqlConnection);

            //执行sql语句
            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}", "编号", "班级名称", "专业编号");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}", sdr["classId"], sdr["className"], sdr["specialtyId"]);
            }

            sqlConnection.Close();
        }


        public void PrintUserInfoAdaper()
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(SqlCommandString, connectionString);

            DataTable dataTable = new DataTable();

            dataAdapter.Fill(dataTable);

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("{0}\t{1}\t{2}", "编号", "班级名称", "专业编号");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}", dataTable.Rows[i]["classId"], dataTable.Rows[i]["className"], dataTable.Rows[i]["specialtyId"]);
            }
        }
    }
}
