﻿
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 姚树昌
{
    class DBHelpers
    {
        public static void PrintStudentInfo()
        {
            string conn = "server = .;database = Student;uid = sa;pwd = 123456";
            SqlConnection sqlconn = new SqlConnection(conn);
            sqlconn.Open();
            var sql = "select * from StudentInfo";
            SqlCommand cmd = new SqlCommand(sql, sqlconn);
            var sqldb = cmd.ExecuteReader();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "学号", "姓名", "年龄", "性别");
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "编号", "姓名", "年龄", "性别");

            while (sqldb.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sqldb["StuId"], sqldb["StuName"], sqldb["StuAge"], sqldb["StuSex"]);
            }
            sqlconn.Close();
        }

        private readonly string conn = "server = .;database = Student;uid = sa;pwd = 123456";
        private readonly string sqlconn = "select * from StudentInfo";

        public void ReachStudentInfo()
        {
            SqlDataAdapter sqldata = new SqlDataAdapter(sqlconn, conn);
            DataTable dt = new DataTable();
            sqldata.Fill(dt);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "StuId", "StuName", "StuAge", "StuSex");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", dt.Rows[i]["StuId"], dt.Rows[i]["StuName"], dt.Rows[i]["StuAge"], dt.Rows[i]["StuSex"]);
            }
        }
    }
}
