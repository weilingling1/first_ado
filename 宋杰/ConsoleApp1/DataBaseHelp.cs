﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{


    class DataBaseHelp
    {

        private readonly string sqlCommandString = "select * from Users";

        private readonly string connectionString = "server=.;database=Test;uid=sa;pwd=123456";

        public static void PrintUserInfo() {

            string connectionString = "server=.;database=Test;uid=sa;pwd=123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();



            var sqlCommandSting = "select * from Users";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandSting, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "Id", "用户名", "密码", "创建日期");

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["Id"], sdr["Username"], sdr["Password"], sdr["CreatedTime"]);

            }

            sqlConnection.Close();
        }



        public void PrintUserInfoAdapter()
        {
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommandString, connectionString);

            DataTable dataTable = new DataTable();

            dataAdapter.Fill(dataTable);

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "Id", "用户名", "密码", "创建日期");

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", dataTable.Rows[i]["Id"], dataTable.Rows[i]["Username"], dataTable.Rows[i]["Password"], dataTable.Rows[i]["CreatedTime"]);
            }
        }
    }
}
