/**创建数据库*/
create database MyCar;
go
use MyCar;
/**创建表*/
create table Car
(
Id int primary key identity(1,1),  --编号
Title nvarchar(128) not null,  --车名
Speed int default(0),  --车速
Info ntext --详细
)

/**添加数据*/
insert into Car(Title,Speed,Info)
select 'BYD',130,'比亚迪' union
select 'BMW',160,'宝马' union
select 'Benz',160,'奔驰' 

/**查询*/
SELECT [Id]
      ,[Title]
      ,[Speed]
      ,[Info]
  FROM [MyCar].[dbo].[Car]
GO